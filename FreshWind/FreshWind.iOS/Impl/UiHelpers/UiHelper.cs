using FreshWind.Contracts.UiHelpers;
using Xamarin.Forms;
using UiHelper = FreshWind.iOS.Impl.UiHelpers.UiHelper;

[assembly: Dependency(typeof(UiHelper))]
namespace FreshWind.iOS.Impl.UiHelpers
{
    public class UiHelper: IUiHelper
    {
        public bool ToolbarIsNotTransparent => false;
    }
}