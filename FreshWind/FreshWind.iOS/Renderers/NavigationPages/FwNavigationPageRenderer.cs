using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FreshWind.iOS.Renderers.NavigationPages;
using FreshWind.Views.NavigationPages;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(FwNavigationPage), typeof(FwNavigationPageRenderer))]
namespace FreshWind.iOS.Renderers.NavigationPages
{
    public class FwNavigationPageRenderer: NavigationRenderer
    {
        private bool _navBarSet;

        private FwNavigationPage _element => (FwNavigationPage) Element;
        
        public override UINavigationBar NavigationBar
        {
            get
            {
                if (_navBarSet)
                    return base.NavigationBar;

                _navBarSet = RefreshHamburgerIcon();
                return base.NavigationBar;
            }
        }

        private bool RefreshHamburgerIcon()
        {
            if (base.NavigationBar.Items != null && base.NavigationBar.Items.Length > 0)
            {
                var item = base.NavigationBar.Items.FirstOrDefault();
                if (item != null)
                {
                    if (item.LeftBarButtonItems != null && item.LeftBarButtonItems.Length > 0)
                    {
                        var leftbutton = item.LeftBarButtonItems[0];

                        leftbutton.Image = new UIImage(_element.MenuIcon);

                        return true;
                    }
                }
            }
            return false;
        }
    }
}