﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshWind.Models.Cashe;
using RestSharp.Portable;
using Rg.Forms.Services.Api;

namespace FreshWind.Services.Rest
{
    public class RestService
    {
        public async Task<IRestResponse<Models.Rest.RestResponse<CacheModel>>> GetAllInfo()
        {
            var request = new RestRequest("all");

            var response = await ApiService.Execute<Models.Rest.RestResponse<CacheModel>>(request);

            return response;
        }
    }
}
