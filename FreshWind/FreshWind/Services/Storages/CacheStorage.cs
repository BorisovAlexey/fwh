﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Akavache;
using FreshWind.Models.Cashe;
using FreshWind.Services.Rest;
using Newtonsoft.Json;

namespace FreshWind.Services.Storages
{
    public class CacheStorage
    {
        private const string CachePropertyKey = "CacheContainer";
        private CacheModel _cache;

        private IBlobCache Storage => BlobCache.UserAccount;

        public event EventHandler<CacheModel> CacheChanged; 

        public async Task<bool> IsCacheExist()
        {
            var createAt = await Storage.GetObjectCreatedAt<CacheModel>(CachePropertyKey);

            if (createAt != null)
                return true;

            return false;
        }

        public async Task<CacheModel> GetCache()
        {
            if (_cache == null)
            {
                try
                {
                    var cache = await Storage.GetObject<CacheModel>(CachePropertyKey);
                    if (cache != null)
                    {
                        _cache = cache;
                        return cache;
                    }
                }
                catch (KeyNotFoundException)
                {
                    return null;
                }
            }

            return _cache;
        }

        public async Task SetCache(CacheModel cache)
        {
            await Storage.InsertObject(CachePropertyKey, cache);
            _cache = cache;
            OnCacheChanged(cache);
        }

        public async Task ClearCache()
        {
            await Storage.InvalidateObject<CacheModel>(CachePropertyKey);

            if (_cache != null)
            {
                _cache = null;
                OnCacheChanged(null);
            }
        }

        protected virtual void OnCacheChanged(CacheModel e)
        {
            CacheChanged?.Invoke(this, e);
        }
    }
}
