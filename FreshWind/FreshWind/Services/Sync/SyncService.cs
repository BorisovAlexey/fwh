﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshWind.Enums.Rest;
using FreshWind.Services.Rest;
using FreshWind.Services.Storages;
using Plugin.Connectivity.Abstractions;

namespace FreshWind.Services.Sync
{
    public class SyncService
    {
        private readonly CacheStorage _cacheStorage;
        private readonly RestService _restService;
        private readonly IConnectivity _connectivity;

        private static bool _isStart;
        private static bool _isLoading;

        public SyncService(CacheStorage cacheStorage, RestService restService, IConnectivity connectivity)
        {
            _cacheStorage = cacheStorage;
            _restService = restService;
            _connectivity = connectivity;
        }

        public void Start(bool updateNow = true)
        {
            if(_isStart)
                return;
            _isStart = true;

            _connectivity.ConnectivityChanged += OnConnectionChanged;

            if(updateNow)
                UpdateCache();
        }

        public void Stop()
        {
            if (!_isStart)
                return;
            _isStart = false;

            _connectivity.ConnectivityChanged -= OnConnectionChanged;
        }

        private void OnConnectionChanged(object sender, ConnectivityChangedEventArgs e)
        {
            UpdateCache();
        }

        private async void UpdateCache()
        {
            if(_isLoading)
                return;
            _isLoading = true;

            var response = await _restService.GetAllInfo();

            if (response.IsSuccess)
            {
                var data = response.Data;
                if (data.StatusCode == StatusCodes.Ok)
                {
                    var result = data.Response;
                    await _cacheStorage.SetCache(result);
                }
            }

            _isLoading = false;
        }
    }
}
