﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FreshWind.Models.Cashe.Informations
{
    public class RoomsModel
    {
        [JsonProperty("highTech")]
        public List<RoomModel> HighTech { get; set; }

        [JsonProperty("country")]
        public List<RoomModel> Country { get; set; }

        [JsonProperty("cottages")]
        public List<RoomModel> Cottages { get; set; }
    }
}
