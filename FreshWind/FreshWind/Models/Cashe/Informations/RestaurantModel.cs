﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FreshWind.Models.Cashe.Informations
{
    public class RestaurantModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("openingTime")]
        public DateTime OpeningTime { get; set; }

        [JsonProperty("closingTime")]
        public DateTime ClosingTime { get; set; }

        [JsonProperty("sevenDaysWeek")]
        public bool SevenDaysWeek { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("images")]
        public List<string> Images { get; set; }
    }
}
