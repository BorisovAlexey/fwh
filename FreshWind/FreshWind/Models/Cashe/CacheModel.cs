﻿using System;
using System.Collections.Generic;
using FreshWind.Models.Cashe.Informations;
using Newtonsoft.Json;

namespace FreshWind.Models.Cashe
{
    public class CacheModel
    {
        [JsonProperty("hotelInformation")]
        public HotelInformationModel HotelInformation { get; set; }

        [JsonProperty("actions")]
        public List<ActionModel> Actions { get; set; }

        [JsonProperty("contacts")]
        public List<ContactModel> Contacts { get; set; }

        [JsonProperty("services")]
        public List<ServiceModel> Services { get; set; }

        [JsonProperty("restaurants")]
        public List<RestaurantModel> Restaurants { get; set; }

        [JsonProperty("rooms")]
        public RoomsModel Rooms { get; set; }
    }
}
