﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshWind.Enums.Rest;
using Newtonsoft.Json;

namespace FreshWind.Models.Rest
{
    public class RestResponse<T>
    {
        [JsonProperty("statusCode")]
        public StatusCodes StatusCode { get; set; }

        [JsonProperty("response")]
        public T Response { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }

    public class RestResponse: RestResponse<object>
    {
    }
}
