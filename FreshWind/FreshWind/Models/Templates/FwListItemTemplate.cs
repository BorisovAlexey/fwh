﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Forms.Data;

namespace FreshWind.Models.Templates
{
    public class FwListItemTemplate<T>: RgListData
    {
        public FwListItemTemplate(T model)
        {
            Model = model;
        }

        public T Model { get; }
    }
}
