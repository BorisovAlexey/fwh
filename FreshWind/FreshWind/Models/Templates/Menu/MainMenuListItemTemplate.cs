﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Forms.Data;

namespace FreshWind.Models.Templates.Menu
{
    public class MainMenuListItemTemplate: RgListData
    {
        public MainMenuListItemTemplate(string icon, string name, Action<MainMenuListItemTemplate> clickHandler)
        {
            Icon = icon;
            Name = name;
            ClickHandler = clickHandler;
        }

        public string Icon { get; set; }
        public string Name { get; set; }
        public Action<MainMenuListItemTemplate> ClickHandler { get; set; }

        public bool IsSelect { get; set; }

        public void OnClick()
        {
            ClickHandler?.Invoke(this);   
        }
    }
}
