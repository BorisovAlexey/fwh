﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FreshMvvm;
using FreshWind.Models.Templates.Menu;
using FreshWind.ViewModels.Pages;
using FreshWind.Views.NavigationPages;
using Plugin.Iconize;
using Rg.FreshMvvm.Extensions;
using Rg.FreshMvvm.PageModels;
using Xamarin.Forms;

namespace FreshWind.ViewModels.MenuPages
{
    public class MainMenuPageModel: RgFreshPageModel
    {
        public List<MainMenuListItemTemplate> PageItems { get; set; }

        public MainMenuListItemTemplate SelectedItem
        {
            get { return null; }
            set
            {
                value.OnClick();
            }
        }

        public override void Init(object initData)
        {
            base.Init(initData);

            PageItems = new List<MainMenuListItemTemplate>
            {
                new MainMenuListItemTemplate("ion-ios-heart-outline", "Акции", OnItemSelected<MainPageModel>),
                new MainMenuListItemTemplate("ion-ios-bell-outline", "Контакты", OnItemSelected<ContactsPageModel>),
                new MainMenuListItemTemplate("ion-ios-home-outline", "Номера / Коттеджи", OnItemSelected<MainPageModel>),
                new MainMenuListItemTemplate("ion-ios-person-outline", "Услуги", OnItemSelected<MainPageModel>),
                new MainMenuListItemTemplate("ion-android-restaurant", "Рестораны", OnItemSelected<MainPageModel>),
                new MainMenuListItemTemplate("ion-ios-information-outline", "Об отеле", OnItemSelected<AboutHotelPageModel>),
                new MainMenuListItemTemplate("ion-ios-partlysunny-outline", "Погода", OnItemSelected<WeatherPageModel>)
            };
        }

        private void OnItemSelected<TPageModel>(MainMenuListItemTemplate item)
            where TPageModel: FreshBasePageModel
        {
            CoreMethods.SwitchSelectedRootPageModel<TPageModel>();
        }
    }
}
