﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshMvvm;
using FreshWind.Models.Cashe;
using FreshWind.Services.Storages;
using Plugin.Connectivity;
using Rg.FreshMvvm.PageModels;

namespace FreshWind.ViewModels.Base
{
    public class FwPageModel: RgFreshPageModel
    {
        private readonly CacheStorage _cacheStorage;

        public FwPageModel()
        {
            _cacheStorage = FreshIOC.Container.Resolve<CacheStorage>();
        }

        protected override void ViewIsAppearing(object sender, EventArgs e)
        {
            base.ViewIsAppearing(sender, e);

            _cacheStorage.CacheChanged += OnCacheChanged;
        }

        protected override void ViewIsDisappearing(object sender, EventArgs e)
        {
            base.ViewIsDisappearing(sender, e);

            _cacheStorage.CacheChanged -= OnCacheChanged;
        }

        private void OnCacheChanged(object sender, CacheModel e)
        {
            CacheChanged(e);
        }

        protected virtual void CacheChanged(CacheModel cache)
        {
            
        }
    }
}
