﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshWind.Models.Cashe;
using FreshWind.Models.Cashe.Informations;
using FreshWind.Models.Templates;
using FreshWind.Services.Rest;
using FreshWind.Services.Storages;
using FreshWind.ViewModels.Base;
using Plugin.Connectivity.Abstractions;
using Rg.Forms.Enumerables.StateContainer;
using Rg.FreshMvvm.Extensions;
using Rg.FreshMvvm.PageModels;
using Xamarin.Forms;

namespace FreshWind.ViewModels.Pages
{
    public class ContactsPageModel: FwPageModel
    {
        private readonly CacheStorage _cacheStorage;

        public States State { get; set; } = States.Loading;

        public List<FwListItemTemplate<ContactModel>> Contacts { get; set; }

        public FwListItemTemplate<ContactModel> SelectedContact
        {
            get { return null; }
            set
            {
                OnContactSelect(value);
            }
        }

        public ContactsPageModel(CacheStorage cacheStorage)
        {
            _cacheStorage = cacheStorage;
        }

        public override void Init(object initData)
        {
            base.Init(initData);

            UpdateContacts();
        }

        protected override void CacheChanged(CacheModel e)
        {
            base.CacheChanged(e);

            UpdateContacts(e);
        }

        private void OnContactSelect(FwListItemTemplate<ContactModel> contactTemplate)
        {
            var phoneNumber = contactTemplate.Model.PhoneNumber;
            var uri = "tel:" + phoneNumber;
            Device.OpenUri(new Uri(uri));
        }

        private async void UpdateContacts(CacheModel cache = null)
        {
            var result = cache ?? await _cacheStorage.GetCache();

            var contacts = result?.Contacts;

            if (contacts == null || contacts.Count == 0)
            {
                State = States.NoData;
            }
            else
            {
                Contacts = result.Contacts.Select(model => new FwListItemTemplate<ContactModel>(model)).ToList();
                State = States.Normal;
            }
        }
    }
}
