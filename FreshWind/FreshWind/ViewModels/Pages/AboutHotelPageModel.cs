﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FreshWind.Models.Cashe;
using FreshWind.Services.Storages;
using FreshWind.ViewModels.Base;
using FreshWind.Views.NavigationPages;
using Xamarin.Forms;

namespace FreshWind.ViewModels.Pages
{
    public class AboutHotelPageModel: FwPageModel
    {
        private readonly CacheStorage _cacheStorage;

        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ICommand OpenDrawerCommand { get; }

        public AboutHotelPageModel(CacheStorage cacheStorage)
        {
            _cacheStorage = cacheStorage;

            OpenDrawerCommand = new Command(OpenDrawer);
        }

        public override void Init(object initData)
        {
            base.Init(initData);

            IsToolbarShow = false;

            UpdateHotelInfo();
        }

        protected override void CacheChanged(CacheModel cache)
        {
            base.CacheChanged(cache);

            UpdateHotelInfo();
        }

        private async void UpdateHotelInfo()
        {
            var result = await _cacheStorage.GetCache();

            var hotelInfo = result?.HotelInformation;

            if (hotelInfo != null)
            {
                ImageUrl = hotelInfo.Images.FirstOrDefault();
                Title = hotelInfo.Title;
                Description = hotelInfo.Description;
            }
        }

        private void OpenDrawer()
        {
            MainMasterDetailPage.ShowMasterPage();
        }
    }
}
