﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FreshWind.Services.Rest;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Rg.Forms.Enumerables.StateContainer;
using Rg.FreshMvvm.PageModels;
using Xamarin.Forms;

namespace FreshWind.ViewModels.Pages
{
    public class WeatherPageModel: RgFreshPageModel
    {
        private const string WeatherUrl = "https://yandex.ru/pogoda/dmitrov";

        public States State { get; set; } = States.Loading;

        public string Url { get; set; }

        public ICommand OnWebViewNavigationCommand { get; }

        public WeatherPageModel()
        {
            OnWebViewNavigationCommand = new Command(OnWebViewNavigation);
        }

        public override void Init(object initData)
        {
            base.Init(initData);

            Url = WeatherUrl;
        }

        protected override void ViewIsAppearing(object sender, EventArgs e)
        {
            base.ViewIsAppearing(sender, e);

            CrossConnectivity.Current.ConnectivityChanged += OnConnectionChange;
            ChangeState();
        }

        protected override void ViewIsDisappearing(object sender, EventArgs e)
        {
            base.ViewIsDisappearing(sender, e);

            CrossConnectivity.Current.ConnectivityChanged -= OnConnectionChange;
        }

        private void OnConnectionChange(object sender, ConnectivityChangedEventArgs e)
        {
            ChangeState();
        }

        private void OnWebViewNavigation()
        {
            if (!CrossConnectivity.Current.IsConnected) 
                State = States.NoInternet;
        }

        private void ChangeState()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                State = States.Normal;
            }
            else
            {
                if (State != States.Normal)
                    State = States.NoInternet;
            }
        }
    }
}
