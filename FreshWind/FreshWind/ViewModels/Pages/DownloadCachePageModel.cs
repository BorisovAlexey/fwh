﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FreshWind.Enums.Rest;
using FreshWind.Services.Rest;
using FreshWind.Services.Storages;
using FreshWind.Services.Sync;
using Plugin.Connectivity.Abstractions;
using Rg.Forms.Enumerables.StateContainer;
using Rg.FreshMvvm.PageModels;
using Xamarin.Forms;

namespace FreshWind.ViewModels.Pages
{
    public class DownloadCachePageModel: RgFreshPageModel
    {
        private readonly CacheStorage _cacheStorage;
        private readonly RestService _restService;
        private readonly SyncService _syncService;
        private readonly IConnectivity _connectivity;

        public States State { get; set; } = States.NoData;

        public ICommand OnDownloadCommand { get; }
        public ICommand OnContinueCommand { get; }

        public DownloadCachePageModel(CacheStorage cacheStorage, RestService restService, SyncService syncService, IConnectivity connectivity)
        {
            _cacheStorage = cacheStorage;
            _restService = restService;
            _syncService = syncService;
            _connectivity = connectivity;

            OnDownloadCommand = new Command(OnDownload);
            OnContinueCommand = new Command(OnContinue);
        }

        public override void Init(object initData)
        {
            base.Init(initData);

            State = _connectivity.IsConnected ? States.NoData : States.NoInternet;
        }

        protected override void ViewIsAppearing(object sender, EventArgs e)
        {
            base.ViewIsAppearing(sender, e);

            _connectivity.ConnectivityChanged += OnConnectionStatusChange;
        }

        protected override void ViewIsDisappearing(object sender, EventArgs e)
        {
            base.ViewIsDisappearing(sender, e);

            _connectivity.ConnectivityChanged -= OnConnectionStatusChange;
        }

        private void OnConnectionStatusChange(object sender, ConnectivityChangedEventArgs e)
        {
            if(State == States.Normal)
                return;

            State = e.IsConnected ? States.NoData : States.NoInternet;
        }

        private async void OnDownload()
        {
            State = States.Loading;

            var response = await _restService.GetAllInfo();

            if (State == States.Loading && response.IsSuccess)
            {
                var data = response.Data;
                if (data.StatusCode == StatusCodes.Ok)
                {
                    var result = data.Response;
                    await _cacheStorage.SetCache(result);
                    State = States.Normal;
                    _syncService.Start(false);
                }
                else
                {
                    State = States.Error;
                }
            }
            else if (State == States.Loading && !response.IsSuccess)
            {
                State = States.Error;
            }
        }

        private async void OnContinue()
        {
            await Task.Delay(300);

            App.Current.MainPage = App.CreateMainMasterDetailPage();
        }
    }
}
