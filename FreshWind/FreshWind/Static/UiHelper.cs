﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshWind.Contracts.UiHelpers;
using Xamarin.Forms;

namespace FreshWind.Static
{
    public static class UiHelper
    {
        private static readonly IUiHelper _container = DependencyService.Get<IUiHelper>();

        public static bool ToolbarIsNotTransparent => _container.ToolbarIsNotTransparent;
    }
}
