﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreshWind
{
    public static class Constants
    {
        public const string BaseUrl = "http://freshwindhotel.ru/remote.api/";
        public const string MainMasterDetailNavigationPageName = "MainMasterDetailNavigationPage";
    }
}
