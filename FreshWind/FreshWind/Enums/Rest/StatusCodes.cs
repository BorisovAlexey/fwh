﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreshWind.Enums.Rest
{
    public enum StatusCodes
    {
        // Success
        Ok = 1,
        // Error
        UnexpectedError = 200,
        ContentNotExist = 201,
        ContentAlreadyExists = 202,
        NotAllDataSent = 203,
        SentDataIsNotCorrect = 204,
        SentDataIsNotValid = 205,
        ConfirmationRequired = 206,
        RetryLater = 207,
        // Fatal Error
    }
}
