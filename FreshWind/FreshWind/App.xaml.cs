﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FreshMvvm;
using FreshWind.Contracts.UiHelpers;
using FreshWind.Services.Rest;
using FreshWind.Services.Storages;
using FreshWind.Services.Sync;
using FreshWind.ViewModels.MenuPages;
using FreshWind.ViewModels.Pages;
using FreshWind.Views.MenuPages;
using FreshWind.Views.NavigationPages;
using FreshWind.Views.Pages;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Rg.Forms.Services.Api;
using Rg.FreshMvvm;
using Rg.FreshMvvm.Extensions;
using Xamarin.Forms;

namespace FreshWind
{
    public partial class App : Application
    {
        private readonly SyncService _syncService;
        private readonly CacheStorage _cacheStorage;

        public App()
        {
            InitializeComponent();

            if (!ApiService.IsInitialize)
            {
                ApiService.Initialize(Constants.BaseUrl);
                ApiService.RequestOptions = new RequestOptions
                {
                    IsShowLoading = false,
                    IsRepeat = false
                };
            }

            RgFreshMvvm.Init();

            RegisterPages();
            RegisterServices();

            _cacheStorage = FreshIOC.Container.Resolve<CacheStorage>();
            _syncService = FreshIOC.Container.Resolve<SyncService>();

            var mainPage = CreateMainMasterDetailPage();

            MainPage = mainPage;

            ShowDownloadPageIfNeed();
        }

        private void RegisterPages()
        {
            FreshIOC.Container.RegisterPageModel<MainMenuPageModel, MainMenuPage>();

            FreshIOC.Container.RegisterPageModel<MainPageModel, MainPage>();
            FreshIOC.Container.RegisterPageModel<WeatherPageModel, WeatherPage>();
            FreshIOC.Container.RegisterPageModel<ContactsPageModel, ContactsPage>();
            FreshIOC.Container.RegisterPageModel<DownloadCachePageModel, DownloadCachePage>();
            FreshIOC.Container.RegisterPageModel<AboutHotelPageModel, AboutHotelPage>();
        }

        private void RegisterServices()
        {
            FreshIOC.Container.Register(CrossConnectivity.Current);

            FreshIOC.Container.Register(DependencyService.Get<IUiHelper>());
            FreshIOC.Container.Register<RestService, RestService>().AsSingleton();
            FreshIOC.Container.Register<CacheStorage, CacheStorage>().AsSingleton();
            FreshIOC.Container.Register<SyncService, SyncService>().AsSingleton();
        }

        public static Page CreateMainMasterDetailPage()
        {
            var masterPage = FreshPageModelResolver.ResolvePageModel<MainMenuPageModel>();
            masterPage.Title = " ";
            var masterDetailPage = new MainMasterDetailNavigationPage();
            masterDetailPage.SetDetailPage(FreshPageModelResolver.ResolvePageModel<AboutHotelPageModel>());
            masterDetailPage.SetMasterPage(masterPage);

            return masterDetailPage;
        }

        private async void ShowDownloadPageIfNeed()
        {
            var isCacheExist = await _cacheStorage.IsCacheExist();

            if(!isCacheExist)
            {
                var downloadCachePage = FreshPageModelResolver.ResolvePageModel<DownloadCachePageModel>();

                MainPage = downloadCachePage;
            }
        }

        private async void StartSyncService()
        {
            if (await _cacheStorage.IsCacheExist())
                _syncService.Start();
        }

        protected override void OnStart()
        {
            // Handle when your app starts

            StartSyncService();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            _syncService.Stop();
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            StartSyncService();
        }
    }
}
