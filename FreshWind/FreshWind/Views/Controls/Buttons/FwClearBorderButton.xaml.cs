﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Forms.Renderers.Controls.Gestures;
using Xamarin.Forms;

namespace FreshWind.Views.Controls.Buttons
{
    public partial class FwClearBorderButton : RgGesturesView
    {
        public readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(FwClearBorderButton), string.Empty, propertyChanged: OnTextChanged);

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public FwClearBorderButton()
        {
            InitializeComponent();
        }

        private static void OnTextChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var self = (FwClearBorderButton) bindable;
            self.TextLabel.Text = (string) newvalue;
        }
    }
}
