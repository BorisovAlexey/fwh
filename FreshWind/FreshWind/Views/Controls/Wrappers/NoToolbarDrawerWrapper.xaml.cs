﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Rg.Forms.Events.Gestures;
using Xamarin.Forms;

namespace FreshWind.Views.Controls.Wrappers
{
    [ContentProperty(nameof(WrapContent))]
    public partial class NoToolbarDrawerWrapper : AbsoluteLayout
    {
        public static readonly BindableProperty PrivateContentProperty = BindableProperty.Create(nameof(PrivateContent), typeof(View), typeof(NoToolbarDrawerWrapper), default(View));

        public View PrivateContent
        {
            get { return (View)GetValue(PrivateContentProperty); }
            set { SetValue(PrivateContentProperty, value); }
        }

        public static readonly BindableProperty WrapContentProperty = BindableProperty.Create(nameof(WrapContent), typeof(View), typeof(NoToolbarDrawerWrapper), propertyChanged: OnWrapContentChanged);

        public View WrapContent
        {
            get { return (View)GetValue(WrapContentProperty); }
            set { SetValue(WrapContentProperty, value); }
        }

        public static readonly BindableProperty NavigationCommandProperty = BindableProperty.Create(nameof(NavigationCommand), typeof(ICommand), typeof(NoToolbarDrawerWrapper));

        public ICommand NavigationCommand
        {
            get { return (ICommand)GetValue(NavigationCommandProperty); }
            set { SetValue(NavigationCommandProperty, value); }
        }

        public static readonly BindableProperty NavigationCommandParameterProperty = BindableProperty.Create(nameof(NavigationCommandParameter), typeof(object), typeof(NoToolbarDrawerWrapper));

        public object NavigationCommandParameter
        {
            get { return (object)GetValue(NavigationCommandParameterProperty); }
            set { SetValue(NavigationCommandParameterProperty, value); }
        }

        public static readonly BindableProperty IconProperty = BindableProperty.Create(nameof(Icon), typeof(ImageSource), typeof(NoToolbarDrawerWrapper), propertyChanged: OnIconChanged);

        [TypeConverter(typeof(ImageSourceConverter))]
        public ImageSource Icon
        {
            get { return (ImageSource)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public NoToolbarDrawerWrapper()
        {
            InitializeComponent();
        }

        private static void OnWrapContentChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var self = (NoToolbarDrawerWrapper) bindable;
            self.Container.Content = (View) newvalue;
        }

        private void OnNavigationClicked(object sender, MotionEventArgs e)
        {
            NavigationCommand?.Execute(NavigationCommandParameter);
        }

        private static void OnIconChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var self = (NoToolbarDrawerWrapper) bindable;

            self.NavigationImage.Source = (ImageSource) newvalue;
        }
    }
}
