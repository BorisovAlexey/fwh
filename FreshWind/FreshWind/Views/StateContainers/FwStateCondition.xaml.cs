﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Forms.Renderers.Controls.StateContainer;
using Xamarin.Forms;

namespace FreshWind.Views.StateContainers
{
    [ContentProperty(nameof(Content))]
    public partial class FwStateCondition : StateCondition
    {
        public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(FwStateCondition), string.Empty, propertyChanged: TitleChanged);
        public static readonly BindableProperty IconNameProperty = BindableProperty.Create(nameof(IconName), typeof(string), typeof(FwStateCondition), string.Empty, propertyChanged: IconNameChanged);
        public static readonly BindableProperty IconColorProperty = BindableProperty.Create(nameof(IconColor), typeof(Color), typeof(FwStateCondition), Color.Default, propertyChanged: IconColorChanged);
        public static readonly BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(FwStateCondition), Color.Default, propertyChanged: TextColorChanged);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public string IconName
        {
            get { return (string)GetValue(IconNameProperty); }
            set { SetValue(IconNameProperty, value); }
        }

        public Color IconColor
        {
            get { return (Color)GetValue(IconColorProperty); }
            set { SetValue(IconColorProperty, value); }
        }

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public FwStateCondition()
        {
            InitializeComponent();
        }

        private static void TitleChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var self = (FwStateCondition) bindable;

            self.TitleLabel.Text = (string) newvalue;
        }

        private static void IconNameChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var self = (FwStateCondition)bindable;

            self.IconView.Text = (string)newvalue;
        }

        private static void IconColorChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var self = (FwStateCondition)bindable;

            self.IconView.TextColor = (Color) newvalue;
        }

        private static void TextColorChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var self = (FwStateCondition)bindable;

            self.TitleLabel.TextColor = (Color)newvalue;
        }
    }
}
