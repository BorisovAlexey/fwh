﻿using FreshMvvm;
using Rg.FreshMvvm.Pages;
using Xamarin.Forms;

namespace FreshWind.Views.Pages
{
    public partial class MainPage : RgFreshContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Title = null;
            Icon = "hamburger.png";
        }
    }
}
