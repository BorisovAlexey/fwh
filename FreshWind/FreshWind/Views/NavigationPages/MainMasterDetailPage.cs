﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshMvvm;
using Xamarin.Forms;

namespace FreshWind.Views.NavigationPages
{
    public class MainMasterDetailPage : MasterDetailPage, IFreshNavigationService 
    {
        public string NavigationServiceName { get; }

        public MainMasterDetailPage() : this(FreshMvvm.Constants.DefaultNavigationServiceName)
        {
        }

        public MainMasterDetailPage(string navigationServiceName)
        {
            NavigationServiceName = navigationServiceName;
            RegisterNavigation();
        }

        public static void ShowMasterPage(string navigationServiceName = FreshMvvm.Constants.DefaultNavigationServiceName)
        {
            OnMasterStateChange(true, navigationServiceName);
        }

        public static void HideMasterPage(string navigationServiceName = FreshMvvm.Constants.DefaultNavigationServiceName)
        {
            OnMasterStateChange(false, navigationServiceName);
        }

        public virtual void SetDetailPage(Page page)
        {
            Detail = page;
        }

        public virtual void SetMasterPage(Page page)
        {
            page.GetModel().CurrentNavigationServiceName = NavigationServiceName;
            Master = page;
        }

        public virtual Task PopToRoot(bool animate = true)
        {
            OnNavigation();
            return Navigation.PopToRootAsync(animate);
        }

        public virtual Task PushPage(Page page, FreshBasePageModel model, bool modal = false, bool animate = true)
        {
            OnNavigation();
            if (modal)
            {
                return Navigation.PushModalAsync(page, animate);
            }
            return Navigation.PushAsync(page, animate);
        }

        public virtual Task PopPage(bool modal = false, bool animate = true)
        {
            OnNavigation();
            if (modal)
            {
                return Navigation.PopModalAsync(animate);
            }
            return Navigation.PopAsync(animate);
        }

        public virtual Task<FreshBasePageModel> SwitchSelectedRootPageModel<T>() where T : FreshBasePageModel
        {
            OnNavigation();
            var page = FreshPageModelResolver.ResolvePageModel<T>();
            SetDetailPage(page);
            return Task.FromResult(page.GetModel());
        }

        public virtual void NotifyChildrenPageWasPopped()
        {
            (Master as NavigationPage)?.NotifyAllChildrenPopped();
            (Detail as NavigationPage)?.NotifyAllChildrenPopped();
        }

        protected virtual void RegisterNavigation()
        {
            FreshIOC.Container.Register<IFreshNavigationService>(this, NavigationServiceName);
        }

        protected virtual void OnNavigation()
        {
            IsPresented = false;
        }

        private static void OnMasterStateChange(bool isPresented, string navigationServiceName)
        {
            var navigationPage = FreshIOC.Container.Resolve<IFreshNavigationService>(navigationServiceName) as MainMasterDetailPage;

            if (navigationPage != null)
            {
                navigationPage.IsPresented = isPresented;
            }
        }
    }
}
