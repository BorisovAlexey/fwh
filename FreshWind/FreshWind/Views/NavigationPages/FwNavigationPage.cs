﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FreshWind.Views.NavigationPages
{
    public class FwNavigationPage: NavigationPage
    {
        public static readonly BindableProperty MenuIconProperty = BindableProperty.Create(nameof(MenuIcon), typeof(string), typeof(FwNavigationPage), string.Empty);

        public string MenuIcon
        {
            get { return (string) GetValue(MenuIconProperty); }
            set { SetValue(MenuIconProperty, value); }
        }

        public FwNavigationPage(Page page, string menuIcon = null): base(page)
        {
            MenuIcon = menuIcon;
        }
    }
}
