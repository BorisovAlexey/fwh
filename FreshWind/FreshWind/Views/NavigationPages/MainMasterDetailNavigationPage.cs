﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshMvvm;
using Xamarin.Forms;

namespace FreshWind.Views.NavigationPages
{
    public class MainMasterDetailNavigationPage: MainMasterDetailPage
    {
        public MainMasterDetailNavigationPage()
        {
            
        }

        public MainMasterDetailNavigationPage(string serviceName):base(serviceName)
        {
            
        }

        public override Task PushPage(Page page, FreshBasePageModel model, bool modal = false, bool animate = true)
        {
            ThrowExceptionIfDetailIsNotNavigationPage();
            OnNavigation();

            if (modal)
            {
                return Navigation.PushModalAsync(CreateContainerPageSafe(page), animate);
            }
            return Detail.Navigation.PushAsync(page, animate);
        }

        public override Task PopToRoot(bool animate = true)
        {
            ThrowExceptionIfDetailIsNotNavigationPage();
            OnNavigation();
            return Detail.Navigation.PopToRootAsync(animate);
        }

        public override Task PopPage(bool modal = false, bool animate = true)
        {
            ThrowExceptionIfDetailIsNotNavigationPage();
            OnNavigation();
            if (modal)
            {
                return Navigation.PopModalAsync(animate);
            }
            return Detail.Navigation.PopAsync(animate);
        }

        public override void SetDetailPage(Page page)
        {
            Detail = CreateContainerPageSafe(page);
        }

        internal Page CreateContainerPageSafe(Page page)
        {
            if (page is NavigationPage || page is MasterDetailPage || page is TabbedPage)
                return page;

            return CreateContainerPage(page);
        }

        protected virtual Page CreateContainerPage(Page page)
        {
            var container = new FwNavigationPage(page, "hamburger.png");

            return container;
        }

        private void ThrowExceptionIfDetailIsNotNavigationPage()
        {
            if (Detail == null)
            {
                throw new NullReferenceException("Detail page can not be null");
            }
            else if(!(Detail is NavigationPage))
            {
                throw new NullReferenceException("Detail page can be NavigationPage");
            }
        }
    }
}
