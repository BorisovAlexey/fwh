﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreshWind.Contracts.UiHelpers
{
    public interface IUiHelper
    {
        bool ToolbarIsNotTransparent { get; }
    }
}
