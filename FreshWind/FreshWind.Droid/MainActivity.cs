﻿using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.OS;
using FFImageLoading.Forms.Droid;
using HockeyApp.Android;
using Rg.Forms.Droid.Helpers;
using Xamarin.Forms.Platform.Android;
using Color = Xamarin.Forms.Color;


namespace FreshWind.Droid
{
    [Activity(
        Label = "FreshWind", 
        Icon = "@drawable/icon",
        Theme = "@style/MainTheme",
        MainLauncher = true, 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            ToolbarResource = Rg.Forms.Droid.Resource.Layout.toolbar;
            TabLayoutResource = Rg.Forms.Droid.Resource.Layout.tabs;

            base.OnCreate(bundle);

            CrashManager.Register(this, Keys.HockeyAppKey);

            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule())
                .With(new Plugin.Iconize.Fonts.IoniconsModule());

            CachedImageRenderer.Init();
            global::Xamarin.Forms.Forms.Init(this, bundle);

            FormsPlugin.Iconize.Droid.IconControls.Init();

            Window.SetSoftInputMode(SoftInput.AdjustResize);
            AndroidBug5497.assistActivity(this);

            var app = new App();

            var mainColor = (Color)App.Current.Resources["MainColor"];

            Window.DecorView.SetBackgroundColor(mainColor.ToAndroid());

            LoadApplication(app);
        }
    }
}

