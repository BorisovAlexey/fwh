using Android.OS;
using FreshWind.Contracts.UiHelpers;
using Xamarin.Forms;
using UiHelper = FreshWind.Droid.Impl.UiHeplers.UiHelper;

[assembly: Dependency(typeof(UiHelper))]
namespace FreshWind.Droid.Impl.UiHeplers
{
    public class UiHelper: IUiHelper
    {
        public bool ToolbarIsNotTransparent
        {
            get
            {
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    return true;
                return false;
            }
        }
    }
}